<link rel='stylesheet' href='web/swiss.css'/>

# Lab session 2: Accessing MongoDB 


## Tutorial

In this tutorial we are going to use [MongoDb Java Driver](https://mongodb.github.io/mongo-java-driver/) to access a MongoDB available over Infrastructure-as-a-Service (IaaS) on [MongoDB Atlas](https://www.mongodb.com/cloud/atlas/). In the tutorial we will see how the use of DSLs help develop cloud systems, focussing on the persistence layer and on cloud computing. In particular, we are going to use [MongoDB Java Driver](https://mongodb.github.io/mongo-java-driver/) from Groovy. 

### Create an account on MongoDB Atlas

First, we need to set up our MongoDB instance. We are going to use mLab, a cloud database provider built atop Infrastructure-as-a-Service (IaaS) providers, so that we do not need to install and configure the database server locally.

* Sign up and create a new account on [MongoDB Atlas](https://www.mongodb.com/cloud/atlas/).
* Build your first cluster, by choosing a cloud provider with a free-tier on a server that is closest to your geographical location: e.g. Ireland if you are in Leicester.
  * While the cluster is being created, read the rest of the worksheet and documentation.
* Click on connect to the cluster and whitelist your IP address (the one shown by default).
* Add a user to your database by providing a `USERNAME` and a `PASSWORD`
* Choose a connection method: `Connect your application`, and select `Java` and `3.6 or later`.
* Copy the connection string in your script for creating the database connection, i.e. something similar to `mongodb+srv://$USERNAME:$PWD@cluster0-aocks.mongodb.net/test?retryWrites=true&w=majority`.
* We are all set. Your MongoDB instance is up and running on the cloud!

You are likely to work from different machines in the lab against the same MongoDB cluster so please whitelist the following IP range: `143.210.71.0/24`(in CIDR format): click on the Cluster you created, and then on the pane on the left, click on `Network Access` and add the IP address `143.210.71.0/24` as shown below.

![whitelisting](whitelist.png)


#### Connecting to your MongoDB cluster

The preferred option is to connect via machines in the lab. Machines can be accessed remotely as explained [here](https://campus.cs.le.ac.uk/gitlab/ab373/CO7X17-19-20#login-remotely-to-a-linux-machine).


##### Wirelessly using _The Cloud

This wireless network should work.

##### Wirelessly using eduroam

To connect to `mLab` using `eduroam`, if you are working from your laptop on campus, you will find that most of the ports are blocked. 

To circumvent this problem, create a SSH tunnel to `xanthus.mcscw3.le.ac.uk` which is a wired machine that will work as a local proxy to the Internet using the following command from a Bash terminal:

	ssh -fNg -L $PORT:mlab.com:$PORT $UOL_USERNAME@xanthus.mcscw3.le.ac.uk

Replace `$PORT` with the port used for your database connection and replace `$UOL_USERNAME` with your University username. The password that you have to enter is your departmental Linux password. Once the tunnel is established there is no confirmation message but the connection should work. 



### Getting started with MongoDb

* Pull the general git repository (clone it first if you haven't done it yet)
* Copy the project `mongodb` under `part1` in the master repository to your private repository
* Import the Gradle project into the Eclipse using `File>Import>Existing Gradle Project`
  * Choose gradle wrapper
  * Choose to overwrite eclipse settings 
* The Gradle build script imports two dependencies: one for compiling groovy code and the other one for using the MongoDB Java driver

```
dependencies {
    compile "org.codehaus.groovy:groovy-all:2.4.17" // ensure it is the same one your IDE is using
	compile 'org.mongodb:mongodb-driver-sync:3.11.0'
}
```

## File MongoDb

[MongoDg.groovy](./src/main/groovy/lab2/MongoDg.groovy) contains example code showing how to implement CRUD operations using a MongoDB store.

The header is as follows:

	import com.gmongo.GMongo
	import com.mongodb.DBCursor
	import com.mongodb.MongoURI
	
	def USERNAME = 
	def PWD = 
	
	def DATABASE = 'test-co7217'
	
	// MAKING THE CONNECTION
	def mongoClient = MongoClients.create("mongodb+srv://$USERNAME:$PWD@cluster0-aocks.mongodb.net/test?retryWrites=true&w=majority");
	
	// GET DATABASE
	def db = mongoClient.getDatabase(DATABASE);


Please configure your credentials by looking at the URI given in the header of your dabatase on MongoDB ATLAS.

The following code checks that the connection can be performed and you should be able to see the names of all the collections on your database: 

	// TESTING CONNECTION
	println 'database: ' + db.getName()
	db.listCollectionNames().each{ println it } 

Check the code in the script for:
* creating documents in a database we will use the following code:
* making queries:
* applying modifications to some documents, the first parameter of the the `update` operation is a filter for finding the document where to apply modifications and the second parameter indicates the type of update to be performed. 
* making upserts
* deleting one or many documents, using a filter.

### Executing the script

In the gradle build script, you will find the task

	task runScript (dependsOn: 'classes', type: JavaExec) {
		main = "lab2.Exercise"
		classpath = sourceSets.main.runtimeClasspath
	}

The main field should point to the script file. From the command line, execute:

	./gradlew runScript

## Exercises 

Solve the following exercises in file [Exercise.groovy](./src/main/groovy/lab2/Exercise.groovy).

### Exercise 1

Using Groovy collections (i.e. using lists and maps), create the following list of documents, where each document is represented as a map and may contain other lists of documents inside.

	 [
	      {
	         "id": "X999_Y999",
	         "from": {
	            "name": "Tom Brady", "id": "X12"
	         },
	         "message": "Looking forward to 2010!",
	         "actions": [
	            {
	               "name": "Comment",
	               "link": "http://www.facebook.com/X999/posts/Y999"
	            },
	            {
	               "name": "Like",
	               "link": "http://www.facebook.com/X999/posts/Y999"
	            }
	         ],
	         "type": "status",
	         "created_time": "2010-08-02T21:27:44+0000",
	         "updated_time": "2010-08-02T21:27:44+0000"
	      },
	      {
	         "id": "X998_Y998",
	         "from": {
	            "name": "Peyton Manning", "id": "X18"
	         },
	         "message": "Where's my contract?",
	         "actions": [
	            {
	               "name": "Comment",
	               "link": "http://www.facebook.com/X998/posts/Y998"
	            },
	            {
	               "name": "Like",
	               "link": "http://www.facebook.com/X998/posts/Y998"
	            }
	         ],
	         "type": "status",
	         "created_time": "2010-08-02T21:27:44+0000",
	         "updated_time": "2010-08-02T21:27:44+0000"
	      }
	   ]

This is an example of a Facebook JSON file which you might see 
when getting data from the Facebook API. It might also be used to 
contain profile information which can be easily shared across 
your system components using the simple JSON format.
(source: [https://www.sitepoint.com/facebook-json-example/](https://www.sitepoint.com/facebook-json-example/))

Then create a collection 'lab2' in a database in your cluster on MongoDB. Use [JSONSlurper](https://groovy-lang.org/json.html) to create JSON format from objects in memory.

### Exercise 2 

Update message field of document with id "X999_Y999" to "Remembering what happened in 2010".

### Exercise 3

Delete the document with id: "id": "X998_Y998"

### Exercise 4

Use [JsonSlurper::parseText()](http://groovy-lang.org/json.html#json_jsonslurper) to parse JSON text into objects in memory, so that there is no need to create objects manually, and use [JsonOutput::toJson()](http://groovy-lang.org/json.html#_jsonoutput) to serialize objects in memory to JSON.

The document [facebook.json](src/main/resources/facebook.json) contains the JSON documents used above. Use `JsonSlurper` to parse JSON from the text directly.

## Sources

* Official API documentation [MongoDb API](https://mongodb.github.io/mongo-java-driver/)
* [Parsing and producing JSON](https://groovy-lang.org/json.html)

***
&copy; Artur Boronat, 2015-19
