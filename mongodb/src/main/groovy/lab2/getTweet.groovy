package lab2



import java.lang.annotation.Documented

import org.bson.Document

import com.mongodb.BasicDBList

import com.mongodb.BasicDBObject

import com.mongodb.client.MongoClients

import com.mongodb.client.model.FindOneAndUpdateOptions

import com.mongodb.client.model.UpdateOptions


import groovy.json.JsonOutput

import groovy.json.JsonSlurper




// MAKING THE CONNECTION

def mongoClient = MongoClients.create("mongodb+srv://sharad922:India%4012345@cluster0-wd05b.mongodb.net/test?retryWrites=true&w=majority");

def DATABASE='test-co7217'

// GET DATABASE

def db = mongoClient.getDatabase(DATABASE);


// TESTING CONNECTION

//println 'database: ' + db.getName()

//db.listCollectionNames().each{ println it }



int countTweetCapitalism =0

int countTweetTrump =0

int countTweetObama=0

int countTweetUSA=0

int countTweetUK=0

int countTweetSealevel=0

int countTweetCarbon=0

int countTweetTemperature=0

int countTweetRainfall=0






def jsonObj =new JsonSlurper()

def file =new File('src/main/resources/climate_change_tweets_all.json')

def climateData = jsonObj.parseText(file.text) as Map[]


def docCount=climateData.size();


for (item in climateData.full_text)

{

def patterncapitalism=/\b${"capitalism"}\b/

def patternObama=/\b${"Obama"}\b/

def patternTrump=/\b${"Trump"}\b/

def patternUSA =/\b${"USA"}\b/

def patternUK =/\b${"UK"}\b/

def patternSealevel=/\b${"sea level"}\b/

def patternCarbon =/\b${"carbon"}\b/

def patternTemperature =/\b${"temperature"}\b/

def patternRainfall =/\b${"rainfall"}\b/




if(item =~ patterncapitalism)

{

countTweetCapitalism++


}

if(item =~ patternObama)

{

countTweetObama++


}

if(item =~ patternTrump)

{

countTweetTrump++


}

if(item =~ patternUSA)

{

countTweetUSA++


}

if(item =~ patternUK)

{

countTweetUK++


}

if(item =~ patternSealevel)

{

countTweetSealevel++


}

if(item =~ patternCarbon)

{

countTweetCarbon++


}

if(item =~ patternTemperature)

{

countTweetTemperature++


}


if(item =~ patternRainfall)

{

countTweetRainfall++


}

}




def capitalismPercentage= (countTweetCapitalism/docCount)*100

def trumpPercentage= (countTweetTrump/docCount)*100

def obamaPercentage = countTweetObama/docCount*100

def usaPercentage= countTweetUSA/docCount*100

def ukPercentage=countTweetUK/docCount*100

def sealevelPercentage =countTweetSealevel/docCount*100

def carbonPercentage =countTweetCarbon/docCount*100

def temperaturePercentage =countTweetTemperature/docCount*100

def rainfallPercentage =countTweetRainfall/docCount*100



println "total tweets: $docCount"

println "capitalism : $countTweetCapitalism"+" "+ "($capitalismPercentage%)"

println "Trump : $countTweetTrump"+" "+ "($trumpPercentage%)"

println "Obama : $countTweetObama"+" "+ "($obamaPercentage%)"

println "USA : $countTweetUSA"+" "+ "($usaPercentage%)"

println "UK : $countTweetUK"+" "+ "($ukPercentage%)"

println "sea level : $countTweetSealevel"+" "+ "($sealevelPercentage%)"

println "carbon : $countTweetCarbon"+" "+ "($carbonPercentage%)"

println "temperature : $countTweetTemperature"+" "+ "($temperaturePercentage%)"

println "rainfall : $countTweetRainfall"+" "+ "($rainfallPercentage%)"