package lab2


import com.mongodb.BasicDBList
import com.mongodb.BasicDBObject
import com.mongodb.client.MongoClients
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.model.UpdateOptions

import groovy.json.JsonOutput
import groovy.json.JsonSlurper


import org.bson.Document


// MAKING THE CONNECTION
def mongoClient = MongoClients.create("mongodb+srv://sharad922:India%4012345@cluster0-wd05b.mongodb.net/test?retryWrites=true&w=majority");
def DATABASE='test-co7217'
// GET DATABASE
def db = mongoClient.getDatabase(DATABASE);

// TESTING CONNECTION
println 'database: ' + db.getName()
db.listCollectionNames().each{ println it }



db.createCollection("climate_change")
def newCol = db.getCollection("climate_change")

def jsonObj =new JsonSlurper()
def file =new File('src/main/resources/climate_change_tweets_all.json')
def climateData = jsonObj.parseText(file.text)
def Document = new Document()
for (item in climateData)

{
def obj=Document.parse(JsonOutput.toJson(item))
newCol.insertOne(obj)
}
print "Insertion is Complete"